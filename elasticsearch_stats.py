#!/usr/bin/env python

import urllib2
import simplejson

u = urllib2.urlopen('http://localhost:9200/_nodes/stats/')
j = u.read()

node_stats = simplejson.loads(j)
#print k['nodes']
stats = {}

for key, value in node_stats['nodes'].iteritems():
# print node_stats['nodes'][key]['indices']['docs']

 stats['indeces_docs_count'] = node_stats['nodes'][key]['indices']['docs']['count']
 stats['indeces_docs_deleted'] = node_stats['nodes'][key]['indices']['docs']['deleted']
 stats['indeces_store_size'] = node_stats['nodes'][key]['indices']['store']['size_in_bytes']
 stats['indexing_index_total'] = node_stats['nodes'][key]['indices']['indexing']['index_total']
 stats['indexing_index_time'] = node_stats['nodes'][key]['indices']['indexing']['index_time_in_millis']
 stats['indexing_delete_total'] = node_stats['nodes'][key]['indices']['indexing']['delete_total']
 stats['indexing_delete_time'] = node_stats['nodes'][key]['indices']['indexing']['delete_time_in_millis']
 stats['get_total'] = node_stats['nodes'][key]['indices']['get']['total']
 stats['get_time'] = node_stats['nodes'][key]['indices']['get']['time_in_millis']
 stats['get_exists_total'] = node_stats['nodes'][key]['indices']['get']['exists_total']
 stats['get_exists_time'] = node_stats['nodes'][key]['indices']['get']['exists_time_in_millis']
 stats['get_missing_total'] = node_stats['nodes'][key]['indices']['get']['missing_total']
 stats['get_missing_time'] = node_stats['nodes'][key]['indices']['get']['missing_time_in_millis']
 stats['search_query_total'] = node_stats['nodes'][key]['indices']['search']['query_total']
 stats['search_query_time'] = node_stats['nodes'][key]['indices']['search']['query_time_in_millis']
 stats['search_fetch_total'] = node_stats['nodes'][key]['indices']['search']['fetch_total']
 stats['search_fetch_time'] = node_stats['nodes'][key]['indices']['search']['fetch_time_in_millis']
 stats['merges_total'] = node_stats['nodes'][key]['indices']['merges']['total']
 stats['merges_time'] = node_stats['nodes'][key]['indices']['merges']['total_time_in_millis']
 stats['merges_total_docs'] = node_stats['nodes'][key]['indices']['merges']['total_docs']
 stats['merges_total_size'] = node_stats['nodes'][key]['indices']['merges']['total_size_in_bytes']
 stats['refresh_total'] = node_stats['nodes'][key]['indices']['refresh']['total']
 stats['refresh_total_time'] = node_stats['nodes'][key]['indices']['refresh']['total_time_in_millis']
 stats['flush_total'] = node_stats['nodes'][key]['indices']['flush']['total']
 stats['flush_total_time'] = node_stats['nodes'][key]['indices']['flush']['total_time_in_millis']
 stats['warmer_total'] = node_stats['nodes'][key]['indices']['warmer']['total']
 stats['warmer_total_time'] = node_stats['nodes'][key]['indices']['warmer']['total_time_in_millis']
 stats['filter_cache_mem_size'] = node_stats['nodes'][key]['indices']['filter_cache']['memory_size_in_bytes']
 stats['id_cache_mem_size'] = node_stats['nodes'][key]['indices']['id_cache']['memory_size_in_bytes']
 stats['fielddata_mem_size'] = node_stats['nodes'][key]['indices']['fielddata']['memory_size_in_bytes']
 stats['completion_size'] = node_stats['nodes'][key]['indices']['completion']['size_in_bytes']
 stats['segments'] = node_stats['nodes'][key]['indices']['segments']['count']
 stats['process_open_files'] = node_stats['nodes'][key]['process']['open_file_descriptors']
 stats['process_cpu_percent'] = node_stats['nodes'][key]['process']['cpu']['percent']
 stats['process_cpu_sys'] = node_stats['nodes'][key]['process']['cpu']['sys_in_millis']
 stats['process_cpu_user'] = node_stats['nodes'][key]['process']['cpu']['user_in_millis']
 stats['process_cpu_total'] = node_stats['nodes'][key]['process']['cpu']['total_in_millis']
 stats['process_mem_resident'] = node_stats['nodes'][key]['process']['mem']['resident_in_bytes']
 stats['process_mem_share'] = node_stats['nodes'][key]['process']['mem']['share_in_bytes']
 stats['process_mem_virtual'] = node_stats['nodes'][key]['process']['mem']['total_virtual_in_bytes']
 stats['jvm_uptime'] = node_stats['nodes'][key]['jvm']['uptime_in_millis']
 stats['jvm_mem_heap_used'] = node_stats['nodes'][key]['jvm']['mem']['heap_used_in_bytes']
 stats['jvm_mem_heap_committed'] = node_stats['nodes'][key]['jvm']['mem']['heap_committed_in_bytes']
 stats['jvm_mem_heap_max'] = node_stats['nodes'][key]['jvm']['mem']['heap_max_in_bytes']
 stats['jvm_mem_non_heap_used'] = node_stats['nodes'][key]['jvm']['mem']['non_heap_used_in_bytes']
 stats['jvm_mem_non_heap_committed'] = node_stats['nodes'][key]['jvm']['mem']['non_heap_committed_in_bytes']
 stats['jvm_threads'] = node_stats['nodes'][key]['jvm']['threads']['count']

#print stats['jvm_threads']

output_str=""

for key, value in stats.items():
# print str(key) + ":" + str(value)
 output_str = output_str + str(key) + ":" + str(value) + " "

#stri = str(stats)
#print stri

print output_str
